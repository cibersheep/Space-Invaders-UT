import QtQuick 2.9
import Ubuntu.Components 1.3
import Morph.Web 0.1
import QtWebEngine 1.7

//import "components"

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'spaceinvaders.cibersheep'
    automaticOrientation: true
    anchorToKeyboard: false

    width: units.gu(45)
    height: units.gu(75)

    property bool nightMode: false
    property bool isLandscape: width > height
    property string lighterColor: "#333333"
    property string darkColor: "#efefef"
/*
    property var settings: Settings {
        property bool hasUpdated: false
    }
*/
    Page {
        id: page

        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        header: PageHeader {
            id: header
            title: "Space Invaders"
            visible: false
        }

        Rectangle {
            anchors.fill: parent
            color: "#111"
        }

        WebView {
            id: webview
            height: isLandscape ? parent.height : parent.height - units.gu(15)

            anchors {
                top: header.visible ? header.bottom : parent.top
                left: parent.left
                right: parent.right
                bottom: parent.bottom
                bottomMargin: isLandscape ? 0 : units.gu(10)
            }

            //context: webcontext
            url: 'www/index.html'

            settings.allowRunningInsecureContent: true

            Component.onCompleted: {
                settings.localStorageEnabled = true
            }

            onJavaScriptConsoleMessage: {
                if (message.match("TODO: Improve this. State.playing true")) {
                    controls.visible = true
                } else if (message.match("TODO: Improve this. State.playing false")) {
                    controls.visible = false
                }
            }
        }

        ProgressBar {
            height: units.dp(3)

            anchors {
                left: parent.left
                right: parent.right
                top: parent.top
            }

            showProgressPercentage: false
            value: (webview.loadProgress / 100)
            visible: (webview.loading && !webview.lastLoadStopped)
        }

        MultiPointTouchArea {
            id: controls
            visible: false
            width: parent.width
            height: isLandscape ? move.height + units.gu(4) : move.height + units.gu(6)

            anchors {
                bottom: parent.bottom
                bottomMargin: isLandscape ? units.gu(6) : units.gu(4)
            }

            property bool aIsDown: false
            property bool lIsDown: false
            property bool rIsDown: false

            signal firePressed
            signal fireReleased
            signal leftPressed
            signal leftReleased
            signal rightPressed
            signal rightReleased

            onFirePressed:   webview.runJavaScript("keyShootPressed = true")
            onFireReleased:  webview.runJavaScript("keyShootPressed = false")
            onLeftPressed:   webview.runJavaScript("keyLeftPressed = true; keyRightPressed = false")
            onLeftReleased:  webview.runJavaScript("keyLeftPressed = false")
            onRightPressed:  webview.runJavaScript("keyLeftPressed = false; keyRightPressed = true")
            onRightReleased: webview.runJavaScript("keyRightPressed = false")

            onTouchUpdated: {
                var r = fire.width / 2
                var r2 = r * r
                var ax = fire.x + r
                var ay = fire.y + r
                var lx = move.x - units.gu(.5)
                var ly = move.y + r
                var rx = move.x + r + r + units.gu(1)
                var ry = move.y + r

                var aDown = false
                var lDown = false
                var rDown = false

                for (var i in touchPoints) {
                    var pt = touchPoints[i]
                    if (pt.pressed) {
                        var dax = ax - pt.x
                        var day = ay - pt.y
                        var dlx = lx - pt.x
                        var dly = ly - pt.y
                        var drx = rx - pt.x
                        var dry = ry - pt.y

                        if (dax * dax + day * day <= r2) {
                            aDown = true
                        }

                        if (dlx * dlx + dly * dly <= r2) {
                            lDown = true
                            moveJoystick.x = Math.min(pt.x -2*r, move.x)
                        }

                        if (drx * drx + dry * dry <= r2) {
                            rDown = true
                            moveJoystick.x  = Math.min(-move.x - r + pt.x, move.x + 2*r)
                        }
                    }
                }

                if (aDown != aIsDown) {
                    if (!aDown) {
                        fireReleased()
                    } else if (!aIsDown) {
                        firePressed()
                    }
                    aIsDown = aDown
                }

                if (lDown != lIsDown) {
                    if (!lDown) {
                        leftReleased()
                    } else if (!lIsDown) {
                        leftPressed()
                    }
                    lIsDown = lDown
                }

                if (rDown != rIsDown) {
                    if (!rDown) {
                        rightReleased()
                    } else if (!rIsDown) {
                        rightPressed()
                    }
                    rIsDown = rDown
                }

            }

            Rectangle {
                id: move

                anchors {
                    bottom: parent.bottom
                    left: parent.left
                    leftMargin: units.gu(6)
                }

                width: units.gu(10)
                height: width
                radius: width * .5
                color: "transparent"

                border {
                    width: units.dp(2)
                    color: "#666"
                }

                Rectangle {
                    id: moveJoystick
                    color: "#666"
                    opacity: controls.lIsDown || controls.rIsDown ? .4 : 0.6
                    width: parent.width * .6
                    height: width
                    radius: width * .5
                    anchors {
                        horizontalCenter: controls.lIsDown || controls.rIsDown ? undefined : parent.horizontalCenter
                        verticalCenter  : parent.verticalCenter
                    }
                }
            }

            Rectangle {
                id: fire

                anchors {
                    bottom: parent.bottom
                    right: parent.right
                    rightMargin: units.gu(6)
                }

                width: units.gu(10)
                height: width
                radius: width * .5
                color: "transparent"

                border {
                    width: units.dp(2)
                    color: "#666"
                }

                Rectangle {
                    color: "#666"
                    opacity: controls.aIsDown ? .4 : 0.6
                    width: controls.aIsDown ? parent.width - units.dp(10) : parent.width - units.gu(1)
                    height: width
                    radius: width * .5
                    anchors.centerIn: parent
                }
            }
        }
    }

/*
    HideHeader {}

    Component {
        id: aboutPage
        About {
            anchors.fill: parent
        }
    }
*/
}
